dynect-zone-backups
===================

This is a shell script that will: 

* Retrieve zone reports from Dynect
* Back them up to `/share/backups/dynect/`
* Clean up zone files from `/share/backups/` that are older than two weeks

## Requirements

This script requires the `bind9tools` and `libxml2-utils packages`, and must be run on a Linux machine.

You will also need to modify the `.dynect.sh` file with your Dynect username and password.

## Usage

Invoke the script with `./get_zone_files.sh <ZONE>`

Running the script without any arguments will return a list of all valid zones.

To get files for all zones, use `all` for the zone argument: `./get_zone_files.sh all`

Retrieved zone files are named `<ZONE>.zonefile` and are saved to `/share/backups/dynect/zones_<YYYYMMDD>/`

Zone files older than 14 days are automatically deleted whenever the script is run.

## TODO:

* Include a switch to choose whether zone file clean up occurs 
* Only perform clean up when getting all zone files
* Dockerize (due to required packages)?
