#!/bin/bash

source /home/vagrant/dynect/dynect-zone-backups/.dynect.sh

DYNURL=https://api.dynect.net/REST/
HEADER=Content-Type:application
SHARE=/share/backups
LOGDIR=/zones_`date "+%Y%m%d"`/
ZFILE=/tmp/zonefile.txt
ZONE=$1

clean_up_old () {
  days=14
  find $SHARE -mtime +$days &>/dev/null
  #find $SHARE -mtime +$days -exec rm -rf {} \;
}
get_zones () {
  token=`curl -s -X POST -H "${HEADER}/yaml" \
  "${DYNURL}Session?user_name=${username}&customer_name=nsidc&password=${password}" |
  grep token |
  sed 's/data: {token: //g' |
  awk -F ',' '{print$1}'`

  zones=`curl -s -X GET -H "${HEADER}/xml" \
    -H "Auth-Token: ${token}" "${DYNURL}Zone/" |
    xmllint --format - |
    grep Zone |
    awk -F '/' '{print$4}'  |
    perl -pe 's/\n/ /g'`
    all_zones+=($zones)
}

get_zone_file () {
  #echo "Getting zone file for ${1}"
  dig $1 @xfrout1.dynect.net -t AXFR +noall +answer > $ZFILE
  named-compilezone -q -f text -F raw -o $ZFILE.raw $1 $ZFILE
  named-compilezone -q -f raw -F text -s relative -o $SHARE$LOGDIR$1.zonefile $1 $ZFILE.raw
  rm $ZFILE*
}

all_zones=()
get_zones

[[ -d $SHARE$LOGDIR ]] || mkdir $SHARE$LOGDIR
   
if [[ -z "$1" ]]; then
  echo "Usage: `basename "$0"` <ZONE>"
  printf "\nValid zones:\n"
  for z in "${all_zones[@]}"; do
    echo "$z"
  done
  exit 1
elif [[ $1 == "all" ]]; then
  for z in "${all_zones[@]}"; do
    get_zone_file $z
  done
else
  get_zone_file $ZONE
fi

clean_up_old
